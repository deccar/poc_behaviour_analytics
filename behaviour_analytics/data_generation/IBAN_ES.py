from schwifty import IBAN
from schwifty import registry


class IBAN_ES(IBAN):

    @classmethod
    def generate(cls, country_code, bank_code, account_code):
        """Generate an IBAN from it's components.

        If the bank-code and/or account-number have less digits than required by their
        country specific representation, the respective component is padded with zeros.

        Examples:

            To generate an IBAN do the following::

                >>> bank_code = '37040044'
                >>> account_code = '532013000'
                >>> iban = IBAN.generate('DE', bank_code, account_code)
                >>> iban.formatted
                'DE89 3704 0044 0532 0130 00'

        Args:
            country_code (str): The ISO 3166 alpha-2 country code.
            bank_code (str): The country specific bank-code.
            account_code (str): The customer specific account-code.
        """

        spec = cls._get_iban_spec(country_code)
        spec['positions']['control_digits'] = [8, 10]
        spec['positions']['account_code'] = [10, 20]
        registry.save('ES', spec)

        bank_code_length = cls._code_length(spec, 'bank_code')
        branch_code_length = cls._code_length(spec, 'branch_code')
        bank_and_branch_code_length = bank_code_length + branch_code_length
        account_code_length = cls._code_length(spec, 'account_code')

        if len(bank_code) > bank_and_branch_code_length:
            raise ValueError(
                "Bank code exceeds maximum size {}".format(bank_and_branch_code_length))

        if len(account_code) > account_code_length:
            raise ValueError(
                "Account code exceeds maximum size {}".format(account_code_length))

        bank_code = bank_code.rjust(bank_and_branch_code_length, '0')
        account_code = account_code.rjust(account_code_length, '0')
        control_digits = cls.__control_digits(bank_code[0:4], bank_code[:8], account_code)
        iban = country_code + '??' + bank_code + control_digits + account_code
        return cls(iban)

    @staticmethod
    def generate_bank_account(cls, bank_code=None, account_code=None):

        generated_iban = cls.generate(country_code='ES', bank_code=bank_code, account_code=account_code)

        return generated_iban

    @staticmethod
    def __control_digits(bank_code, branch_code, account_code):
        def proc(digits):
            result = 11 - sum(int(d) * 2 ** i for i, d in enumerate(digits)) % 11
            return result if result < 10 else 11 - result

        return '%d%d' % (proc('00' + bank_code + branch_code), proc(account_code))

    @staticmethod
    def _get_iban_spec(country_code):
        try:
            return registry.get('iban')[country_code]
        except KeyError:
            raise ValueError("Unknown country-code '{}'".format(country_code))

    @staticmethod
    def _code_length(spec, code_type):
        start, end = spec['positions'][code_type]
        return end - start
