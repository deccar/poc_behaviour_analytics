# -*- coding: utf-8 -*-

import os
import numpy as np

from flask import Flask, jsonify, request

from ..model import IBAN_to_numeric
from ..model import OperationsModel

working_dir = os.getcwd()
input_file = os.path.join(working_dir, 'data.csv')
print("Input data file: ", input_file)

app = Flask("behaviour_analytics")
app.config["model"] = OperationsModel(input_file)


@app.route("/api/transfers")
def home():
    model = app.config["model"]

    user_id = request.args.get("user_id")
    orig_account = request.args.get("orig_account")
    dest_account = request.args.get("dest_account")
    amount = float(request.args.get("amount"))

    orig_account_numeric = IBAN_to_numeric(orig_account)
    dest_account_numeric = IBAN_to_numeric(dest_account)

    transfer_data = np.hstack([user_id, orig_account_numeric, dest_account_numeric, amount])
    prediction = model.predict_transfer_group(transfer_data)

    predicted_group, prediction_error, cluster_mean_dist, pred_score = prediction

    return jsonify(items=[pred_score],
                   group=[str(predicted_group)],
                   _links={
                       "self": {
                           "method": request.method,
                           "href": "/api/transfers?user_id=" + user_id
                                   + "&orig_account=" + orig_account
                                   + "&dest_account=" + dest_account
                                   + "&amount=" + str(amount),
                           "api": "/api"
                       }
                   })


@app.route("/api")
def api():
    return jsonify([
        {
            "attributeId": "luxiam/transfers",
            "service": "/api/transfers",
            "parameters": [
                {
                    "field": "user_id",
                    "category": "subject"
                },
                {
                    "field": "orig_account",
                    "category": "action"
                },
                {
                    "field": "dest_account",
                    "category": "action"
                },
                {
                    "field": "amount",
                    "category": "action"
                }

            ]}
    ])


def main(debug_mode):
    app.run(debug=debug_mode)
