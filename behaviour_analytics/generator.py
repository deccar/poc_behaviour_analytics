#!/usr/bin/python3
# coding=utf-8
import os

from collections import namedtuple
from faker import Factory
from datetime import timedelta

# from behaviour_analytics.behaviour_analytics.data_generation import BankAccount
from behaviour_analytics.data_generation import BankAccount

NUM_USERS = 100
MAX_ACCOUNTS_PER_USER = 5
MIN_TRANSFERS_PER_ACCOUNT = 20
MAX_TRANSFERS_PER_ACCOUNT = 200

MIN_TRANSFER_AMOUNT = 25
MAX_TRANSFER_AMOUNT = 25000

BBVA_BANK_CODE = '0182'

SEED = 2305


def main():
    fake = Factory.create('es_ES')
    fake.seed(SEED)
    fake.add_provider(BankAccount)

    users_list = list()
    transfers = list()

    UserData = namedtuple('UserTuple', ['user_id', 'bank_accounts'])

    for user_id in range(1, NUM_USERS):
        num_user_accounts = fake.random.randint(1, MAX_ACCOUNTS_PER_USER)
        user_accounts = list()

        for account_id in range(0, num_user_accounts):
            rand_user_account = fake.generate_bank_account(bank_code=BBVA_BANK_CODE)
            user_accounts.append(rand_user_account)

        # user = User(user_id, fake.name(), user_accounts)

        user = UserData(user_id, user_accounts)
        users_list.append(user)

    for user in users_list:

        num_transfers = fake.random.randint(MIN_TRANSFERS_PER_ACCOUNT, MAX_TRANSFERS_PER_ACCOUNT)

        for transfer_id in range(MIN_TRANSFERS_PER_ACCOUNT, num_transfers):

            transfer_dest_account = fake.generate_bank_account()
            transfer_timestamp = fake.date_time_this_year(before_now=True, after_now=False, tzinfo=None)
            transfer_amount = fake.random.uniform(MIN_TRANSFER_AMOUNT, MAX_TRANSFER_AMOUNT)

            user_accounts = user.bank_accounts
            print(user_accounts)
            rand_acc_index = fake.random.randint(0, len(user_accounts) - 1)
            rand_user_account = user_accounts[rand_acc_index]
            transfer = format_tuple('N', user.user_id, transfer_timestamp, rand_user_account,
                                    transfer_dest_account, transfer_amount)

            transfers.append(transfer)
            print(transfer)

            # generate a monthly pattern 10% of times
            if fake.random.random() > 0.9:
                for i in range(0, 6):
                    days_diff = fake.random.randint(29, 31)
                    hours_diff = fake.random.randint(1, 8)
                    sec_diff = fake.random.randint(1, 3600)

                    diff = timedelta(days=days_diff, hours=hours_diff, seconds=sec_diff)

                    transfer_timestamp = transfer_timestamp - diff
                    transfer_amount = fake.random.uniform(max(0, transfer_amount - 50), transfer_amount + 50)

                    transfer = format_tuple('M', user.user_id, transfer_timestamp, rand_user_account,
                                            transfer_dest_account, transfer_amount)

                    transfers.append(transfer)
                    print(transfer)

            # generate weekly pattern 5% of times
            if fake.random.random() < 0.05:
                for i in range(0, fake.random.randint(4, 25)):
                    weeks_diff = fake.random.randint(2, 6)
                    days_diff = fake.random.randint(1, 3)
                    hours_diff = fake.random.randint(1, 8)
                    sec_diff = fake.random.randint(1, 3600)

                    diff = timedelta(weeks=weeks_diff, days=days_diff, hours=hours_diff, seconds=sec_diff)

                    transfer_timestamp = transfer_timestamp - diff
                    transfer_amount = fake.random.uniform(max(0, transfer_amount - 1000), transfer_amount + 1000)

                    transfer = format_tuple('W', user.user_id, transfer_timestamp, rand_user_account,
                                            transfer_dest_account, transfer_amount)

                    transfers.append(transfer)
                    print(transfer)

    working_dir = os.getcwd()

    output_file = os.path.join(working_dir, 'data.csv')
    with open(output_file, 'w') as outfile:
        outfile.writelines(transfers)

    print("Output data file: ", output_file)


def format_tuple(label, user_id: str, transfer_timestamp, transfer_orig_account: str,
                 transfer_dest_account: str, transfer_amount: float) -> str:
    return ','.join((label, str(user_id), str(transfer_timestamp), str(transfer_orig_account),
                     str(transfer_dest_account), "{0:.2f}".format(transfer_amount))) + '\n'


if __name__ == '__main__':
    main()
