import os
import numpy as np
import plotly as py
import plotly.graph_objs as go

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import style
from matplotlib import pyplot as plt
from matplotlib import colors as mplcol
from random import shuffle

from .utils import label_attribute


def plotly_3d_scatter(input_data, centroids_list, n_clusters, file_name):
    # print(input_data[:, 0:2].tolist())
    # print(list(map(''.join, input_data[:, 0:2].tolist())))

    print("plotting ", file_name, "...")
    orig_accounts = label_attribute(list(map(''.join, input_data[:, 0:2].tolist())))
    dest_accounts = label_attribute(list(map(''.join, input_data[:, 2:4].tolist())))
    amounts = input_data[:, 4]
    labels = input_data[:, 5]

    graph_data = np.column_stack([orig_accounts, dest_accounts, amounts, labels])

    colors = ["g", "r", "b", "m", "c", "y"]
    all_colors = list(mplcol.cnames.keys())
    all_colors = [x for x in all_colors if x not in colors]
    shuffle(all_colors)
    colors += all_colors

    data = list()
    for i in range(n_clusters):
        label_i_records = graph_data[graph_data[:, -1] == str(i)]

        li_orig_acc = label_i_records[:, 0]
        li_dest_acc = label_i_records[:, 1]
        li_amounts = label_i_records[:, 2]

        trace = go.Scatter3d(
            x=li_orig_acc,
            y=li_dest_acc,
            z=li_amounts,
            text=list(map(lambda x: 'Cluster ' + str(x), (list(range(n_clusters))))),
            mode='markers',
            marker=dict(
                color=colors[i],
                size=7,
                symbol='circle',
                line=dict(
                    color='rgb(204, 204, 204)',
                    width=1
                ),
                opacity=0.9
            )
        )
        data.append(trace)

    # draw the centroids
    # for centroid in centroids_list:
    #     trace = go.Scatter3d(
    #         x=centroid[0],
    #         y=centroid[1],
    #         z=centroid[2],
    #         mode='markers',
    #         marker=dict(
    #             color="k",
    #             size=5,
    #             symbol='circle',
    #             line=dict(
    #                 color='rgb(204, 204, 204)',
    #                 width=1
    #             ),
    #             opacity=0.9
    #         )
    #     )
    #     data.append(trace)

    layout = go.Layout(
        margin=dict(l=0, r=0, b=0, t=0)
    )

    fig = go.Figure(data=data, layout=layout)

    working_dir = os.getcwd()
    graphs_dir = os.path.join(working_dir, 'user_models_graphs')

    if not os.path.isdir(graphs_dir):
        os.makedirs(graphs_dir)

    full_path_file_name = os.path.join(graphs_dir, file_name)
    py.offline.plot(fig, filename=full_path_file_name, auto_open=False, image_filename=file_name)
    print("done plotting ", full_path_file_name)


# Unused functions

def plot_result(model, arr_acc):
    style.use("ggplot")

    X = arr_acc[:, 3]
    print(X)

    plt.interactive(False)
    # plt.scatter(X, y_pred)
    # plt.show()
    labels = model.labels_
    centroids = model.cluster_centers_
    print(centroids)
    colors = ["g.", "r.", "b.", "m.", "c.", "y.", "b."]
    for i in range(len(X)):
        plt.plot(arr_acc[i][4], arr_acc[i][5], colors[labels[i]])
    plt.scatter(centroids[:, 0], centroids[:, 1], marker="x", s=150, linewidths=5, zorder=10)
    plt.show()


def plotly_3d_clustering(arr_acc, labels, centers):
    scatter = dict(
        mode="markers",
        name="y",
        type="scatter3d",
        x=arr_acc[:, 4], y=arr_acc[:, 5], z=arr_acc[:, 6],
        marker=dict(size=2, color="rgb(23, 190, 207)")
    )
    clusters = dict(
        alphahull=7,
        name="y",
        opacity=0.1,
        type="mesh3d",
        x=arr_acc[:, 4], y=arr_acc[:, 5], z=arr_acc[:, 6]
    )
    layout = dict(
        title='3d point clustering',
        scene=dict(
            xaxis=dict(zeroline=False),
            yaxis=dict(zeroline=False),
            zaxis=dict(zeroline=False),
        )
    )
    fig = dict(data=[scatter, clusters], layout=layout)
    # Use py.iplot() for IPython notebook
    py.offline.plot(fig, filename='3d_clustering.html')


def plot_clusters_3d(data, labels):
    style.use("ggplot")

    fig = plt.figure(figsize=plt.figaspect(0.4))
    fig.suptitle('Scatter plot of cluster group assignments')

    dim_0 = data[:, 0]
    ax = fig.add_subplot(121, projection='3d')
    # ax = Axes3D(fig)

    #    ax.scatter(x, y, z, c='r', marker='o')
    colors = ["g", "r", "b", "m", "c", "y"]
    for i in range(len(dim_0)):
        orig_accounts = float(data[i][0])
        dest_accounts = float(data[i][1])
        amounts = float(data[i][2])
        ax.scatter(orig_accounts, dest_accounts, amounts, c=colors[labels[i]])

    ax.set_xlabel('Source Account')
    ax.set_ylabel('Destination Account')
    ax.set_zlabel('Amount')

    # ss = fig.add_subplot(122, projection='3d')
    # for i in range(len(dim_0)):
    #     ss.scatter(data[i][0], data[i][4], data[i][5], c=colors[labels[i]])
    #
    # ss.set_xlabel('Day')
    # ss.set_ylabel('Source Account')
    # ss.set_zlabel('Destination Account')

    plt.show()


__all__ = ["plotly_3d_scatter"]
