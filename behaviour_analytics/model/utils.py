import csv
import numpy as np

from math import ceil
from sys import byteorder
from dateutil import parser
from operator import attrgetter
from sklearn import metrics
from sklearn import preprocessing


def read_input_data(input_filename):
    transfers = list()
    with open(input_filename, newline='\n') as csv_file:
        input_data = csv.reader(csv_file, delimiter=',')

        for row in input_data:
            transfers.append(row)

    return np.array(transfers)


def preprocess_date(timestamp_string):
    ts_datetime = parser.parse(timestamp_string)
    ts_date = ts_datetime.date()

    dt_day = attrgetter('day')(ts_date)
    dt_month = attrgetter('month')(ts_date)
    dt_year = attrgetter('year')(ts_date)
    dt_weekday = ts_datetime.isoweekday()

    return np.array([dt_weekday, dt_day, dt_month, dt_year])


def label_attribute(attr_col):
    le = preprocessing.LabelEncoder()

    le.fit(attr_col)
    labeled_attr = le.transform(attr_col)

    return labeled_attr


def IBAN_to_numeric(attr_col):
    if isinstance(attr_col, list):
        numeric_attr = [[str(string_to_numeric(elem[0:2])) + elem[2:14], elem[14:]] for elem in
                        attr_col]
    else:
        numeric_attr = str(string_to_numeric(attr_col[0:2])) + attr_col[2:14], attr_col[14:]
    return numeric_attr


def encode_array_attr(arr_acc):
    oh_enc = preprocessing.OneHotEncoder()
    oh_enc.fit(arr_acc)

    return oh_enc.transform(arr_acc)


def string_to_numeric(s: str):
    return int.from_bytes(s.encode(), byteorder)


def numeric_to_string(n: int):
    return n.to_bytes(ceil(n.bit_length() / 8), byteorder).decode()


def mean_cluster_silhouettes(arr_acc, n_clusters, labels):
    # sil_score = metrics.silhouette_score(np.array(arr_acc), np.array(model.labels_))
    # print(sil_score)

    samples_scores = metrics.silhouette_samples(np.array(arr_acc), np.array(labels))
    # print(samples_scores)

    labeled_arr = np.column_stack([labels, samples_scores])
    mean_cluster_silhouettes = [np.mean(labeled_arr[labeled_arr[:, 0] == i], axis=0) for i in
                                range(n_clusters)]
    print(mean_cluster_silhouettes)

    return mean_cluster_silhouettes

__all__ = ["numeric_to_string", "string_to_numeric", "encode_array_attr", "IBAN_to_numeric",
           "label_attribute", "preprocess_date", "read_input_data"]
