import random
import numpy as np


def Wk(mu, clusters):
    K = len(mu)
    return sum([np.linalg.norm(mu[i] - c) ** 2 / (2 * len(c)) for i in range(K) for c in clusters[i]])


def bounding_box(X):
    v_min = np.amin(X.tolist(), axis=0).tolist()
    v_max = np.amax(X.tolist(), axis=0).tolist()
    box = np.dstack((v_min, v_max))
    box = tuple(map(tuple, box[0]))
    return box


def gap_statistic(X, max_K):
    box_coords = bounding_box(X)

    # Dispersion for real distribution
    B = 10
    ks = range(1, max_K)
    Wks = np.zeros(len(ks))
    Wkbs = np.zeros(len(ks))
    sk = np.zeros(len(ks))

    for ind_k, k in enumerate(ks):
        mu, clusters = find_centers(X, k)
        Wks[ind_k] = np.log(Wk(mu, clusters))

        # Create B reference datasets
        BWkbs = np.zeros(B)
        for i in range(B):
            Xb = []
            for n in range(len(X)):
                Xb.append(list(map(lambda x: random.uniform(x[0], x[1]), box_coords)))
            Xb = np.array(Xb)
            mu, clusters = find_centers(Xb, k)
            BWkbs[i] = np.log(Wk(mu, clusters))
        Wkbs[ind_k] = sum(BWkbs) / B
        sk[ind_k] = np.sqrt(sum((BWkbs - Wkbs[ind_k]) ** 2) / B)
    sk = sk * np.sqrt(1 + 1 / B)

    return ks, Wks, Wkbs, sk


def cluster_points(X, mu):
    clusters = {}
    for x in X:
        best_mu_key = min([(i[0], np.linalg.norm(x - mu[i[0]])) for i in enumerate(mu)], key=lambda t: t[1])[0]
        try:
            clusters[best_mu_key].append(tuple(x))
        except KeyError:
            clusters[best_mu_key] = [x]
    return clusters


def reevaluate_centers(mu, clusters):
    new_mu = []
    keys = sorted(clusters.keys())
    for k in keys:
        new_mu.append(np.mean(clusters[k], axis=0))
    return new_mu


def has_converged(mu, old_mu):
    return set([tuple(a) for a in mu]) == set([tuple(a) for a in old_mu])


def find_centers(X, K):
    # Initialize to K random centers
    old_mu = random.sample(X.tolist(), K)
    mu = random.sample(X.tolist(), K)
    clusters = {}
    while True:
        # Assign all points in X to clusters
        clusters = cluster_points(X, mu)
        # Reevaluate centers
        mu = reevaluate_centers(old_mu, clusters)
        if not has_converged(mu, old_mu):
            old_mu = mu
        else:
            break
    return mu, clusters


def compute_k(Y, max_k):
    ks, logWks, logWkbs, sk = gap_statistic(Y, max_k)

    best = [1, 0]
    for i in range(1, ks.stop - 1):
        gap_i = logWkbs[i - 1] - logWks[i - 1]
        gap_j = logWkbs[i] - logWks[i]

        dif = abs(gap_i - (gap_j + sk[i]))

        if dif + sk[i-1] >= best[1]:
            best = [i, dif]
        else:
            break
    return best[0]


__all__ = ["compute_k"]


# def main():
#     X = np.array([[float('21317380182451572'), float('0000058311'), float('21317662100644478'), float('0000021888'),
#                    float('13497.23')],
#                   [float('21317380182451572'), float('0000058311'), float('21317080241343800'), float('0091637309'),
#                    float('17278.06')],
#                   [float('21317080182276443'), float('0000030355'), float('21317531516871027'), float('0000001170'),
#                    float('12184.04')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('21746.17')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('22594.68')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('21606.84')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('21862.45')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('21369.14')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('21543.43')],
#                   [float('21317350182884578'), float('0063134071'), float('21317081549283310'), float('0000004005'),
#                    float('22412.25')]])
#
#     Y = np.array([[float('21317640182633231'), float('0299388020'), float('21317171502062445'), float('0000018395'),
#                    float('11140.43')],
#                   [float('21317450182727459'), float('0000001479'), float('21317121479545022'), float('0000081117'),
#                    float('1642.82')],
#                   [float('21317450182727459'), float('0000001479'), float('21317841554378831'), float('0081368946'),
#                    float('1391.84')],
#                   [float('21317450182727459'), float('0000001479'), float('21317871509257721'), float('0000774118'),
#                    float('3070.11')],
#                   [float('21317450182727459'), float('0000001479'), float('21317989000157378'), float('0000007491'),
#                    float('19679.53')],
#                   [float('21317640182633231'), float('0299388020'), float('21317670241644563'), float('0000053520'),
#                    float('6780.73')],
#                   [float('21317450182727459'), float('0000001479'), float('21317100196844916'), float('0000001343'),
#                    float('21753.31')],
#                   [float('21317640182633231'), float('0299388020'), float('21317811548181518'), float('0434518007'),
#                    float('7047.96')],
#                   [float('21317450182727459'), float('0000001479'), float('21317200019166374'), float('0000060699'),
#                    float('13493.35')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18989.77')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18982.01')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18942.13')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18936.23')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18928.03')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18881.89')],
#                   [float('21317450182727459'), float('0000001479'), float('21317051530485563'), float('0871893613'),
#                    float('18922.86')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('904.04')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('924.93')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('940.26')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('969.5')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('985.3')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('990.06')],
#                   [float('21317450182727459'), float('0000001479'), float('21317900083318592'), float('0000000799'),
#                    float('957.99')],
#                   [float('21317640182633231'), float('0299388020'), float('21317430167597744'), float('7858657957'),
#                    float('12362.31')],
#                   [float('21317450182727459'), float('0000001479'), float('21317441508667564'), float('0958428591'),
#                    float('4951.45')],
#                   [float('21317450182727459'), float('0000001479'), float('21317790229157287'), float('0000068766'),
#                    float('4144.54')]]
#
#                  )


# if __name__ == '__main__':
#     main()
