import numpy as np

from scipy import spatial as scsp
from sklearn.cluster import KMeans

from .plotter import plotly_3d_scatter
from .utils import *
from .k_selection import *


class OperationsModel:
    def __init__(self, input_filename: str, max_elems: int = -1):
        np.set_printoptions(threshold=np.inf)
        self.models = self.init_models(input_file=input_filename, max_items=max_elems)

    def __find_model_by_user_id(self, user_id: int):
        return self.models[self.models[:, 0] == user_id][0, 1]

    def predict_transfer_group(self, transfer_data):
        user_model = self.__find_model_by_user_id(transfer_data[0])

        typed_transfer_data = list(map(float, transfer_data[1:]))
        list_transfer_data = np.array(typed_transfer_data).reshape((1, -1))

        predicted_group = user_model.predict(list_transfer_data)[0]

        # cluster_distances = user_model.transform(arr_attr)
        dist = scsp.distance.cdist(np.array([typed_transfer_data]),
                                   np.array([user_model.cluster_centers_[predicted_group]]))

        # return [predicted_group, cluster_distances[0, predicted_group]]
        cluster_mean_dist = user_model.dists[predicted_group]
        return [predicted_group, dist[0][0], cluster_mean_dist, dist[0][0] / cluster_mean_dist]

    @staticmethod
    def create_model(num_clusters: int, attr_array):
        model = KMeans(n_clusters=num_clusters, random_state=170)
        model.fit(attr_array)
        return model

    @staticmethod
    def init_models(input_file: str, max_items: int):

        transfers = read_input_data(input_file)
        if (max_items > 0):
            transfers = transfers[:max_items]
        user_ids = set(transfers[:, 1])
        print(user_ids)

        models = []
        for user_id in user_ids:
            # print(user_id)

            userid_transfers = transfers[transfers[:, 1] == user_id]
            # print(userid_transfers)

            col_orig_accounts = userid_transfers[:, 3]
            col_dest_accounts = userid_transfers[:, 4]

            # arr_date_attr = list(map(self.preprocess_date, userid_transfers[:, 2]))
            col_num_orig_account = IBAN_to_numeric(list(col_orig_accounts))
            col_num_lab_dest_account = IBAN_to_numeric(list(col_dest_accounts))

            col_amount = list(map(float, userid_transfers[:, 5]))
            # norm_amount_attr = preprocessing.MinMaxScaler().fit_transform(col_amount_attr)
            # print(norm_amount_attr)

            arr_acc = np.column_stack([  # arr_date_attr,
                np.array(col_num_orig_account),
                np.array(col_num_lab_dest_account),
                np.array(col_amount)])
            # print(arr_acc)

            # enc_arr_acc = self.encode_array_attr(arr_acc)
            # print(enc_arr_acc.toarray())

            num_arr_acc = [list(map(float, tr_elem)) for tr_elem in arr_acc]

            k = compute_k(np.array(num_arr_acc), min(10, len(num_arr_acc)))
            print("El modelo ", user_id, "utilizará k=", k)

            model = OperationsModel.create_model(k, arr_acc)

            # Puedo calcular el silhouette factor medio de cada cluster generado, pero
            # no puedo calcular el silhouette de una sola muestra así que no sirve de mucho
            # a la hora de ofrecer una medida porcentual de precisión de las predicción
            # self.mean_cluster_silhouettes(arr_acc, model.n_clusters, model.labels_)

            # for i in range(model.n_clusters):
            #    label_i_records = arr_acc[arr_acc[:, -1] == str(i)]
            #    model.dists = scsp.distance.cdist([label_i_records], [model.cluster_centers_[i]])

            model.dists = []

            print('model ' + user_id)

            lab_arr_acc = np.column_stack([num_arr_acc, model.labels_])

            for i in range(model.n_clusters):
                label_i_records = lab_arr_acc[lab_arr_acc[:, -1] == float(i)][:, 0:5]
                # print('cluster ' + str(i))
                # print(label_i_records.tolist())
                dist = scsp.distance.cdist(label_i_records.tolist(), np.array([model.cluster_centers_[i]]))
                mean_dist = sum(dist.flatten()) / len(label_i_records)
                model.dists.append(mean_dist)

            # self.plot_clusters_3d(arr_acc, model.labels_)
            plotly_3d_scatter(
                np.column_stack([arr_acc, model.labels_]),
                model.cluster_centers_,
                model.n_clusters,
                '3d-scatter-user-' + user_id + '_k_' + str(k) + '.html')
            models.append([user_id, model])

        return np.array(models)


# for testing purposes only
# def main():
#     om = OperationsModel()
#     pepe = om.IBAN_to_numeric(['ES6401826332310299388020', 'ES4501827274590000001479'])
#     print(list(pepe))
#     # models = om.init_models()
#     # print(models[:, 0])
#
#
# if __name__ == '__main__':
#     main()

__all__ = ["OperationsModel"]
