La forma de instalar el software es descargar del repositorio los fuentes en formato zip

https://bitbucket.org/deccar/poc_behaviour_analytics/get/master.zip

Y ejecutar desde una shell el siguiente comando:


$ pip install master.zip


Esto instalará en el sistema un paquete llamado behaviour_analytics, resolviendo e instalando todas las dependencias de paquetes que tiene el proyecto, y habilitará automáticamente la posibilidad de ejecutar los siguientes comandos:


*$ ba-data-generator:* 	

Sirve para generar un dataset, que se escribirá en el directorio de trabajo actual (desde el que se invoque el comando) con el nombre data.csv.

*$ ba-run-api:*

Desencadena la construcción de los modelos de datos y despliega el servicio web, que queda a la escucha de peticiones una vez éstos han sido construidos.

Como subproducto genera también las gráficas de los modelos de usuario en el directorio user_models_graphs que, si no existe, se creará en el directorio de trabajo actual.