from distutils.core import setup
from os.path import join, dirname
from setuptools import find_packages


with open(join(dirname(__file__), 'requirements.txt')) as f:
    required = f.read().splitlines()

setup(
    name='behaviour_analytics',
    version='0.0.1',
    install_requires=required,
    packages=find_packages(),
    include_package_data=True,
    entry_points={'console_scripts': [
        'ba-data-generator = behaviour_analytics.generator:main',
        'ba-run-api = behaviour_analytics.run_api:main',
    ]},
    description='Smart XACML Policy Information Point (PIP) based on bank transfer data',
    url='https://bitbucket.org/deccar/poc_behaviour_analytics',
    license='BSD',
    author='Daniel Hernandez',
    author_email='deccar@gmail.com',
)
